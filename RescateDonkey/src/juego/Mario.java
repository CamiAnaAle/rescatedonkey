package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Mario {
	
	private double x;
	private double y;
	private double angulo;
	private int salto;
	private int vidas;
	private Image imag1;
	private Image imag2;
	private Image imag3;
	private Image imag4;
	private	int estado;
	public Mario(int x,int y) {
		
		this.x=x;
		this.y=y;
		this.angulo=0.0;
		this.estado=0;
	    imag1 = Herramientas.cargarImagen("marioparado40x60.png");	
		imag2 = Herramientas.cargarImagen("marioder40x60.png");
		imag3 = Herramientas.cargarImagen("marioizq40x60.png");
		imag4 = Herramientas.cargarImagen("mariosube40x60.png");
		this.salto=0;
		this.vidas=5;
	}
	
	public void setX(double x) {
		this.x = x;
	}

	public void dibujarMario(Entorno entorno) {
		
		if(estado==0)
			entorno.dibujarImagen(imag1, this.x, this.y, this.angulo);
		if(estado==1)
			entorno.dibujarImagen(imag2, this.x, this.y, this.angulo);

		if(estado==2)
			entorno.dibujarImagen(imag3, this.x, this.y, this.angulo);
		if(estado==3)
			
			entorno.dibujarImagen(imag4, this.x, this.y, this.angulo);

			}
	
	public void moverIzquierda() 
	{
		
		if(this.x>20)
		{
			this.x -= 3.0;
			this.estado=2;
		}
		else this.estado=0;
		
	}
	
	public void moverDerecha() 
	{
		if(this.x<795)
		{
			this.x += 3.0;
			this.estado=1;
		}
		else this.estado=0;
		
		
	}
	public void moverArriba() {
		this.y-=5;
		if(this.salto>=1)
		this.estado=0;
		else this.estado=3;
		
	}
	public void moverAbajo() {
		this.y+=5;
		this.estado=0;
	}
	
	
	public void subirEscalera() {
		this.y -= 2.0;

		this.estado=3;
	}
	
	public void bajarEscalera() {
		this.y += 2.0;

		this.estado=3;
	}
	
	public boolean colisionaCon(Escalera e) {
		if (this.x<=e.getX()+10 && this.x>=e.getX()-10 && this.y<e.getY()+58 && this.y>e.getY()-118) {
			return true;
		}
		return false;
	}
	
	public boolean puedeCaminar(Viga v) {
		if (this.y<=(v.getY()-42) && this.y>=(v.getY()-46)){
			return true;
		}
		return false;
	}
	
	public boolean puedeSubir(Escalera e) {
		if (this.y<=e.getY()+32 && this.y>=e.getY()-118) {
			return true;
		}
		return false;
	}
	
	public boolean puedeBajar(Escalera e) {
		if (this.y<=e.getY()+28 && this.y>=e.getY()-132) {
			return true;
		}
		return false;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	public boolean marioColicionViga(Viga[] viga) {
		for (int i = 0; i < viga.length; i++) {
			if(viga[i].primera) {
				if(this.y<=viga[i].getY()-44 && this.y>=viga[i].getY()-70) {
					return true;
				}
				
			}
			else {
				if(this.y<=viga[i].getY()-44 && this.y>=viga[i].getY()-60 && this.x<=viga[i].getX()+320 &&this.x>=viga[i].getX()-320) {
				return true;
				}
				
			}
		}
		return false;
	}
	public boolean marioColicionVigaU(Viga viga) {
		if(viga.primera) {
			if(this.y<=viga.getY()-44 && this.y>=viga.getY()-70) {
				return true;
			}
			return false;
			
		}
		else {
			if(this.y<=viga.getY()-44 && this.y>=viga.getY()-60 && this.x<=viga.getX()+320 &&this.x>=viga.getX()-320) {
				return true;
			}
			return false;
			
		}
		
	}
	public int getvidas()
	{
		return this.vidas;
	}
	public void setsalto()
	{
		this.salto++;
	}
	public void setsalto(int salto)
	{
		this.salto=salto;
	}
	public int getsalto()
	{
		return this.salto;
	}
	public void setvidas()

	{
		this.vidas--;
	}
	public void setY(double y) {
		this.y = y;
	}
	public boolean marioEstaEscalera(Escalera[] e) {
		for (int i = 0; i < e.length; i++) {
			if (this.x<=e[i].getX()+10 && this.x>=e[i].getX()-10 && this.y<e[i].getY()+58 && this.y>e[i].getY()-118) {
				return true;
			}
			
		}
		return false;
	}
	public boolean colicionBarriles(Barril[] barriles) {
		if(this.x==50 && this.y==540) {
			return false;
		}
		for (int i = 0; i < barriles.length; i++) {
			
			if(!(barriles[i]==null)) {
			if(barriles[i].get_Y() >= this.y - (60 / 2) && barriles[i].get_Y() <= this.y + (60 / 2) &&
			barriles[i].get_X() >= this.x - (40 / 2) && barriles[i].get_X() <= this.x + (40/ 2)) {
				return true;
			}
			}
			
		}
		return false;
	}
	public boolean colicionDonkey(Donkey d) {
		if(d.getY() >= this.y - (60 ) && d.getY() <= this.y + (60 ) &&
				d.getX() >= this.x - (40 ) && d.getX() <= this.x + (40)) {
			return true;
		}
	
	return false;
}	
}
