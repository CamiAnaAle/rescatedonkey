package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Donkey {
	private double x;
	private double y;
	private double angulo;
	private Image imag1;
	
	public Donkey(int x,int y) {
		this.x=x;
		this.y=y;
		this.angulo=0.0;
		imag1 = Herramientas.cargarImagen("monoparado120x130.png");
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void dibujarDonkey(Entorno entorno) {
		entorno.dibujarImagen(imag1, this.x, this.y, this.angulo);
}

}
