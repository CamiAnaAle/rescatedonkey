package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Viga {
	
	private double x;
	private double y;
	private double angulo;
	private	Image imag1;
	private Image imag2;
	private double maximo;
	private double minimo;
	public boolean primera;

	public Viga(double x,double y,double minimo,double maximo) {
		
		this.x=x;
		this.y=y;
		this.minimo=minimo;
		this.maximo=maximo;
		this.angulo=0.0;
		imag1 = Herramientas.cargarImagen("viga600x30 2.jpeg");
		imag2 = Herramientas.cargarImagen("viga800x30 2.jpeg");
		this.primera=false;
	}

	public void dibujarViga(Entorno entorno) {
		entorno.dibujarImagen(imag1, x, y,this.angulo);
	}
	
	public void dibujarVigaMario(Entorno entorno) {
		entorno.dibujarImagen(imag2, x, y,this.angulo);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getMaximo() {
		return maximo;
	}

	public double getMinimo() {
		return minimo;
	}
	

	
	
	
}


