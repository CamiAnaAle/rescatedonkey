package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Viga[] vigas;
	private Escalera[] escaleras;
	private Barril[] barriles;
	private Mario mario;

	private Barril[] barrilrodar;
	private Donkey donkey;
	private Image imag2,imag3,imag4;
	private int contador;
	
	
	// Variables y métodos propios de cada grupo
	// ...
	
	Juego()
	{
		// Inicializa el objeto entorno

		imag2=Herramientas.cargarImagen("fondo.png");

		imag3=Herramientas.cargarImagen("fondodeperdido.jpg");

		imag4=Herramientas.cargarImagen("fondohasganado.jpg");
		this.entorno = new Entorno(this, "RescateDonkey - Grupo Apellido1 - Apellido2 -Apellido3 - V0.01", 800, 600);
		this.mario= new Mario(50,540); // 50 en x por convencion y 540 por la diferencia de la viga que tiene 30 de alto (600-30)
		this.donkey= new Donkey(180,65); // posicionado el extremo izquierdo superior (medida del mono, 120 ancho; 130 alto)
		this.vigas= new Viga[4]; // 4 vigas
		this.escaleras=new Escalera[4];
		this.barriles=new Barril[4];
		
		// formula para vigas en general "no utlizado" (entorno.alto()-(30/2))/this.vigas.length;
		double posy=146; // alto de pantalla le resto 15 de viga para luego dividirlo por la cantidad de vigas y se puedan imprimir parejo
		double posx;
		double posey; // posicion y de la escalera
		double maximo, minimo;
		this.barrilrodar=new Barril[1];
		for (int i = 0; i <this.vigas.length-1; i++) { // recorre de 0 a 2 incluido
			posx=500;
			if(i%2==0) {
				posx=300; // en posicion par le doy x=300 para para graficarlo del lado izquierdo
			}
			minimo=posx-280.0;
			maximo=600.0;
			this.vigas[i]=new Viga(posx,posy,minimo,maximo); // creacion de nueva viga en cada vector
			posy+=146; // incremento del paso para coordenada "y" de la viga
		}
		
		this.vigas[vigas.length-1]=new Viga(400,posy,20.0,780);// la ultima viga le paso en x=400 ya que la imagen de esa viga el ancho=800; seria la viga de mario
		this.vigas[vigas.length-1].primera=true;
		posey=219; // la primera posicion 'y' de la escalera centrada ya que el entorno dibujar imagen pide xey del centro de la imagen  
		this.escaleras[0]=new Escalera(270,72);
		for (int i = 1; i < escaleras.length; i++) {
			posx=570; // en la viga pegada a la pared derecha la diferencia del ancho de la viga (800-600)+30  
			if(i%2==0) { 
				posx=230; // en la viga pegada a la izquierda le resto el ancho de la viga (600-30)
			}
			this.escaleras[i]=new Escalera(posx,posey);
			posey+=146; // incrementacion para posicionar la 'y'
		}
		this.barriles[0]=new Barril(30,60,0);
		this.barriles[1]=new Barril(30,120,0);
		this.barriles[2]=new Barril(90,60,0);
		this.barriles[3]=new Barril(90,120,0);
		double x=150,y=136;
		for(int i=0;i<barrilrodar.length;i++)
		{
			x+=200;
			this.barrilrodar[i]=new Barril(x,y,5);
		}
		
		// Inicializar lo que haga falta para el juego
		// ...
		// Inicia el juego!
		this.entorno.iniciar();
		this.contador=0;
	}
	public Barril[] nuevoBarril(Barril[] barril) {
		Barril[] nuevo= new Barril[barril.length+1];
		for (int i = 0; i < barril.length; i++) {
			nuevo[i]=barril[i];
			
		}
		nuevo[nuevo.length-1]=new Barril(150,136,5);
		return nuevo;
	}
	public boolean terminado() {
		if(this.mario.getvidas()<1 || this.mario.colicionDonkey(this.donkey)) {
			return true;
		}
		return false;
	}
	
	

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	/* (sin Javadoc)
	 * @see entorno.InterfaceJuego#tick()
	 */
	public void tick()
	{
		// Procesamiento de un instante de tiempo
		// ...
		if(!this.terminado()) {
		if(mario.colicionDonkey(donkey)) {
			System.out.println("gano");
		}
		if(mario.colicionBarriles(barriles) || mario.colicionBarriles(barrilrodar)) {
		
			mario.setvidas();
			this.mario.setY(540);
			this.mario.setX(50);
			
		}
		
		else {
			
			entorno.dibujarImagen(imag2, 500, 300, 0,1);

			
			entorno.cambiarFont("arial", 30, Color.BLUE);
			entorno.escribirTexto("vidas:" + this.mario.getvidas(), 700, 50);
			
			for (int i = 0; i < this.vigas.length; i++) {
				escaleras[i].dibujarEscalera(entorno); // se dibujan posterior a la viga solo en las 3 vigas
				if(i!=vigas.length-1) {
					vigas[i].dibujarViga(entorno); // dibuja las 3 primeras vigas
				}
				else {
					vigas[i].dibujarVigaMario(entorno); //  dibuja solo una vez la viga de mario que seria la ultima posicion del arreglo
				}
			}
		
	
		
			donkey.dibujarDonkey(entorno);
			mario.dibujarMario(entorno);
			if(mario.getsalto()>0 && mario.getsalto()<13) {
				mario.moverArriba();
				mario.setsalto();
				if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) ) {
					mario.moverIzquierda();
				}
				if (entorno.estaPresionada(entorno.TECLA_DERECHA) ) {
					mario.moverDerecha();
				}
			}
			else {
					if(mario.marioColicionViga(this.vigas)) {
						if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) ) {
							mario.moverIzquierda();
						}
						if (entorno.estaPresionada(entorno.TECLA_DERECHA) ) {
							mario.moverDerecha();
						}
						if(entorno.sePresiono(entorno.TECLA_ESPACIO)) {
							mario.setsalto(1);
						}
						if(mario.getsalto()>0 && mario.getsalto()<13) {
							mario.moverArriba();
							mario.setsalto();
						}
					}
					else {
			
						if(mario.getsalto()>0 && mario.getsalto()<13) {
							mario.moverArriba();
							if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) ) {
								mario.moverIzquierda();
							}
							if (entorno.estaPresionada(entorno.TECLA_DERECHA) ) {
								mario.moverDerecha();
							}
							mario.setsalto();
						}
						if(mario.getsalto()>=13) {
							if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) ) {
								mario.moverIzquierda();
							}
							if (entorno.estaPresionada(entorno.TECLA_DERECHA) ) {
								mario.moverDerecha();
							}
							mario.setsalto(0);
						}
						else {
							if(!mario.marioEstaEscalera(escaleras) && mario.getY()<540) {
								if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) ) {
									mario.moverIzquierda();
								}
								if (entorno.estaPresionada(entorno.TECLA_DERECHA) ) {
									mario.moverDerecha();
								}
								mario.moverAbajo();
							}
				
				}
			}
			}
			if(mario.getsalto()==13) {
				mario.setsalto(0);
			}
				for (int i = 0; i < vigas.length; i++) {
					if(mario.marioColicionVigaU(vigas[i]) && !mario.marioEstaEscalera(escaleras) && mario.getsalto()==0 ) {
						mario.setY(vigas[i].getY()-44);
					}
				}
			
			
			for (int i = 0; i < this.escaleras.length; i++) {
				
				if (mario.colisionaCon(escaleras[i]) && (mario.puedeSubir(escaleras[i]) || mario.puedeBajar(escaleras[i])) ){
					mario.dibujarMario(entorno);
				
					if (entorno.estaPresionada(entorno.TECLA_ARRIBA) && mario.puedeSubir(escaleras[i])) {
						mario.subirEscalera();
					}
					if (entorno.estaPresionada(entorno.TECLA_ABAJO) && mario.puedeBajar(escaleras[i])) {
						mario.bajarEscalera();
					}
				}	
			}
		}
		
		
			
			for(int i=0;i<barrilrodar.length;i++)
			{
				if(!(barrilrodar[i]==null)) {
				barrilrodar[i].dibujarBarril(entorno,false);
				barrilrodar[i].avanzar(vigas);
				}
			}for (int i = 0; i < this.barriles.length; i++) {
				barriles[i].dibujarBarril(entorno,true);
			}
			
	
		this.contador++;
		if(this.contador%200==0) {
			this.barrilrodar=this.nuevoBarril(barrilrodar);
		}
		}
		if(mario.getvidas()<1) {
			//poner pantalla de perdio
			entorno.dibujarImagen(imag3, 420, 310, 0,0.8);
		}
		else {
			if(mario.colicionDonkey(donkey)) {
			//poner pantalla de gano
				entorno.dibujarImagen(imag4, 375, 299, 0,1.56);
			}
	}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
