package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;


public class Barril {

	private double x;
	private double y;
	private Image imag1;
	private Image imag2;
	private double viga;
	private double angulo;
	private boolean estado;
	public Barril(double x,double y,double angulo) {
		this.x=x;
		this.y=y-20;
		this.viga=0;
		this.estado=true;
		this.angulo=angulo;
		//120 viga 1 - 265 viga 2-410 viga 3 -475 viga 4
		
		imag1 = Herramientas.cargarImagen("barrilo.png");
		imag2 = Herramientas.cargarImagen("barril20x20.jpeg");
	}

	public void dibujarBarril(Entorno entorno, boolean movimiento) {
		
		if(movimiento)
			{
		if(this.estado)
		entorno.dibujarImagen(imag1, this.x, this.y, this.angulo);
		else entorno.dibujarImagen(imag1, this.x, this.y, this.angulo,1);
			}
		else {
			
			if(this.estado)
				entorno.dibujarImagen(imag2, this.x, this.y, this.angulo);
				else entorno.dibujarImagen(imag2, this.x, this.y, this.angulo,1);
			
		}
		
}
		
public double get_Y() {
	return this.y;
}

public double get_X()
{
	
	return this.x;
}

public void avanzar(Viga[] vigas) {
	 this.estado=false;
	if(this.x<=vigas[0].getMaximo() && (this.viga==0 || this.viga==2 ))
		{
			  this.x+=3;
			  return;
		}
		else {

		if(this.x>=(vigas[1].getX()-(vigas[1].getMaximo()/2))&& this.viga==1)
			{
				 this.x-=3;
				  return;
				  
			}
			if(this.viga==3 && this.x>=0)
				{
				this.x-=3;
				
					return;
				}
			this.viga++;
			
				this.y+=vigas[0].getY();
			
		}
}

}
