package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Escalera {

	private double x;
	private double y;
	private double angulo;
	private	Image imag1;
	
	public Escalera(double x, double y) {
		
		this.x=x;
		this.y=y;
		this.angulo=0.0;
		imag1=Herramientas.cargarImagen("escalera30x116.png");
	}
	
	public void dibujarEscalera(Entorno entorno) {
		entorno.dibujarImagen(imag1, x, y, angulo);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
}